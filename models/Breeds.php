<?php

namespace app\models;

class Breeds extends \yii\redis\ActiveRecord
{
    public function attributes()
    {
        return ['id', 'name'];
    }

    public function safeAttributes()
    {
        return ['name'];
    }
}
