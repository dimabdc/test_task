<?php

namespace app\controllers;

use yii\rest\ActiveController;

class ListController extends ActiveController
{
    public $modelClass = 'app\models\Breeds';

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['view'], $actions['options']);
        return $actions;
    }
}
