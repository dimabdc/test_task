<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>USAGE:</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-1">GET</div><div class="col-sm-11"><a href="<?= Yii::$app->request->getHostInfo() ?>/list"><?= Yii::$app->request->getHostInfo() ?>/list</a></div>
                </div>
                <div class="row">
                    <div class="col-sm-1">POST</div><div class="col-sm-11"><a href="<?= Yii::$app->request->getHostInfo() ?>/list"><?= Yii::$app->request->getHostInfo() ?>/list</a></div>
                </div>
                <div class="row">
                    <div class="col-sm-1">DELETE</div><div class="col-sm-11"><a href="<?= Yii::$app->request->getHostInfo() ?>/list"><?= Yii::$app->request->getHostInfo() ?>/list/:id</a></div>
                </div>
                <div class="row">
                    <div class="col-sm-1">PATCH</div><div class="col-sm-11"><a href="<?= Yii::$app->request->getHostInfo() ?>/list"><?= Yii::$app->request->getHostInfo() ?>/list/:id</a></div>
                </div>
            </div>
        </div>

    </div>
</div>
